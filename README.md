# sentinel-spring-boot

#### 介绍
alibaba  sentinel 限流中间件在Spring Boot的实践 多多关注公众号：码农小胖哥 

#### 说明

配套文章：[使用Sentinel对Spring MVC接口进行限流](https://mp.weixin.qq.com/s/CTVjZMWUArn1J8bzDvtJHg)

- `SentinelControllerAdvice` 全局限流异常处理。
- `GlobalSentinelWebMvcConfiguration` 全局限流拦截器配置。
- `SentinelAspectConfiguration` 使得`@SentinelResource`生效，同时硬编码了一个针对`/foo/bar`的限流规则。
- `FooController` 测试控制器。

#### 配合控制台须知

禁用`SentinelControllerAdvice`，同时去掉`FooController#bar()`的`@SentinelResource`注解，然后在控制台进行限流的动态配置。
