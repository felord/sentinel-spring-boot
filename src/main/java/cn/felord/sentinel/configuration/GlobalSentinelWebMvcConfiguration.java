package cn.felord.sentinel.configuration;

import com.alibaba.csp.sentinel.adapter.spring.webmvc.SentinelWebInterceptor;
import com.alibaba.csp.sentinel.adapter.spring.webmvc.config.SentinelWebMvcConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class GlobalSentinelWebMvcConfiguration implements WebMvcConfigurer {


    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        SentinelWebMvcConfig sentinelWebMvcConfig = new SentinelWebMvcConfig();
        //指定 请求方法 POST  GET 等等
        sentinelWebMvcConfig.setHttpMethodSpecify(true);
        //默认使用统一Web上下文   如果希望支持链路关系的流控策略则应该设置为false
        sentinelWebMvcConfig.setWebContextUnify(true);
        // 统一的 BlockException 处理  FlowException（BlockException） 会被 JVM 的 UndeclaredThrowableException 包裹一层  某种原因并不能捕获到异常
//        sentinelWebMvcConfig.setBlockExceptionHandler(new DefaultBlockExceptionHandler());
        // 用来标识来源 可针对性的对特定客户端的请求进行流控   limitApp
//        sentinelWebMvcConfig.setOriginParser(request -> request.getParameter("app"));

        //对原始的URL进行处理，比如去掉锚点之类的    /foo/bar?a=3#title  ->   /foo/bar?a=3
//        sentinelWebMvcConfig.setUrlCleaner( );
        registry.addInterceptor(new SentinelWebInterceptor(sentinelWebMvcConfig)).addPathPatterns("/**");
    }
}
